import torch
import train_model_Unet
from build_model import *
from torch.utils.data import DataLoader
from torchvision import transforms

from data_loader import *
import numpy as np
import cv2


a = UNet(3, 1)
a.load_state_dict(torch.load('Weights_BCE_Dice_InvDice/cp_bce_flip_lr_04_no_rot1_0.7307780086994171.pth.tar'))

transformations_test = transforms.Compose([transforms.Resize((512,512)),transforms.ToTensor()])  

teste_set = LungSegVal(transforms = transformations_test)
test = DataLoader(teste_set, batch_size = 4)

# preds = []
i = 0
for xx, yy, name in test:
  pred = a(xx)
  pred = torch.nn.functional.sigmoid(pred)
  pred = pred.detach().numpy()[0, 0, :, :]
  
  cv2.imwrite('predicts/'+str(i)+'.png', pred)

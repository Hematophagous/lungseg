# from torch.utils.data.dataset import Dataset
# from torchvision import transforms
from skimage import io, transform
from PIL import Image
import os
import numpy as np


class Dataset(object):
    def __init__():
        print("hm")

class LungSegTrain(Dataset):
    def __init__(self, path='../data/train/X', transforms=None):
        self.path = path
        self.list = os.listdir(self.path)
        self.list = [self.list[x] for x in range(len(self.list)//10)]
        # print(len(self.list))
        # quit()
        self.transforms = transforms
        
    def __getitem__(self, index):
        # stuff
        image_path = '../data/train/X/'
        mask_path = '../data/train/Y/'
        image = Image.open(image_path+self.list[index])
        image = image.convert('RGB')
        mask = Image.open(mask_path+self.list[index])
        mask = mask.convert('L')
        if self.transforms is not None:
            image = self.transforms(image)
            mask = self.transforms(mask)
        # If the transform variable is not empty
        # then it applies the operations in the transforms with the order that it is created.
        return (image, mask)

    def __len__(self):
        return len(self.list) # of how many data(images?) you have

class LungSegVal(Dataset):
    def __init__(self, path='../data/test/X', transforms=None):
        self.path = path
        self.list = [os.listdir(self.path)[0]]

        self.transforms = transforms
        
    def __getitem__(self, index):
        # stuff
        image_path = '../data/test/X/'
        mask_path = '../data/test/Y/'
        image_name = self.list[index]
        image = Image.open(image_path+self.list[index])
        image = image.convert('RGB')
        mask = Image.open(mask_path+self.list[index])
        mask = mask.convert('L')
        if self.transforms is not None:
            image = self.transforms(image)
            mask = self.transforms(mask)
        # If the transform variable is not empty
        # then it applies the operations in the transforms with the order that it is created.
        return (image, mask, image_name)

    def __len__(self):
        return len(self.list)

# if __name__ == '__main__':
#     LungSegTrain()
import torch
from build_model import *

from torch.utils.data import DataLoader
from torchvision import transforms

from data_loader import *
import numpy as np
import cv2
from PIL import Image

import gc
import glob
import platform
import time


# print("-===========")
cuda = torch.cuda.is_available()
print("Cuda", cuda)

a = UNet(3, 1)
a.load_state_dict(torch.load('Weights_BCE_Dice_InvDice/cp_bce_flip_lr_04_no_rot20_0.05325031706265041.pth.tar', map_location = torch.device('cpu')))
# os.system("pause")


transformations_test = transforms.Compose([transforms.Resize((1024,1024)),transforms.ToTensor()])  

# teste_set = LungSegVal(transforms = transformations_test)
# test = DataLoader(teste_set, batch_size = 1)
# print("+===========")	
ROOT = '/content/gdrive/My Drive/resized_png/'
# ROOT = 'C:/Users/gabriel/Desktop/Base/resized_png/'
global_dir = ['test/', 'train/']
global_sub_dir = ['NORMAL/', 'PNEUMONIA/']
OUTPUT = '/content/gdrive/My Drive/VGG_UNET_OUTPUT/'

def Split(f):
	if platform.system() == 'Windows':
		sym  = '\\'
	else:
		sym  = '/'

	return f.split(sym)[-1]

class LungInput(object):
	def __init__(self, path, transforms=None):
		self.path = path
		self.transforms = transforms

	def __getitem__(self, index):
		name = Split(self.path)
		image = Image.open(self.path)
		image = image.convert('RGB')

		if self.transforms is not None:
			image = self.transforms(image)

		return (image, image, name)

	def __len__(self):
		return 1

# preds = []
# i = 0
# for xx, yy, name in test:
for d in global_dir:
	for sd in global_sub_dir:
		print(ROOT+d+sd)
		filenames = glob.glob(ROOT+d+sd+'*.png')
		predicted = glob.glob(OUTPUT+d+sd+'*.png')
		predicted = [Split(x) for x in predicted]
		i = 0
		leng = str(len(filenames))
		for f in filenames:
			name = Split(f)
			if not name in predicted:
				print("PREDICTING FILE ["+str(i)+'/'+leng+']',  f)
				time.sleep(.2)
				t_set = LungInput(f, transformations_test)
				test = DataLoader(t_set, batch_size = 1)
				gc.collect()	
				for xx, yy, nam in test:
					pred = a(xx)
					pred = torch.nn.functional.sigmoid(pred)
					pred = pred.detach().numpy()[0, 0, :, :]
					cv2.imwrite(OUTPUT+d+sd+nam[0], 255*pred)
			i += 1
				# quit() 
